#ifndef NTPSHM_NTPSHM_H
#define NTPSHM_NTPSHM_H

#include <aSubRecord.h>
#include <time.h>
#include <sys/shm.h>

namespace ntpshm {
  const key_t NTPD_SEG0 = 0x4E545030;

  struct NtpShmSegment {
    int mode;
    int count;

    // The EVR timestamp
    time_t stampSec;
    int stampUsec;

    // The system time when it was acquired
    time_t rxSec;
    int rxUsec;

    int leap;
    int precision;
    int nsamples;
    int valid;

    // Add nano secs
    unsigned        stampNsec;     /* Unsigned ns timestamps */
    unsigned        rxNsec;   /* Unsigned ns timestamps */
    int pad[8];
  };

  // open the NTP shared memory segment
  void setup();

  // the segment
  NtpShmSegment* seg = nullptr;
  
  // put segment data into asub values
  void update(aSubRecord* asub, NtpShmSegment shm);
}

int ntpshmRead(aSubRecord* asub);

#endif