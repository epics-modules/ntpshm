#include "ntpshm/ntpshm.h"
#include <alarm.h>
#include <epicsExport.h>
#include <iostream>
#include <recGbl.h>
#include <registryFunction.h>
#include <stdexcept>

namespace ntpshm {
void setup() {
  if (seg) {
    return;
  }

  int segid = 2;   // TODO: parameterize this?
  int mode = 0666; // TODO: document me
  int shmid = shmget((key_t)(NTPD_SEG0 + segid), sizeof(NtpShmSegment), mode);

  if (shmid == -1) {
    throw std::runtime_error(
      "ntpshm::setup(): unable to get identifier for NTP shared memory segment "
    );
  }

  seg = (NtpShmSegment*)shmat(shmid, 0, 0);
  if (seg == (NtpShmSegment*)-1) {
    seg = nullptr;
    throw std::runtime_error(
      "ntpshm::setup(): unable to attach to NTP shared memory segment"
    );
  }
}

template <typename T> void setOutput(void* output, T value) {
  ((T*)output)[0] = value;
}

double combinedTime(time_t sec, long usec, long nsec) {
  double time = sec;
  time += (nsec / 1000 == usec) ? (nsec * 1e-9) : (usec * 1e-6);
  return time;
}

void update(aSubRecord* asub) {

  // the evr timestamp
  setOutput(asub->vala, seg->stampSec);
  setOutput(asub->valb, seg->stampUsec);
  setOutput(asub->valc, seg->stampNsec);

  // the system time when it was acquired
  setOutput(asub->vald, seg->rxSec);
  setOutput(asub->vale, seg->rxUsec);
  setOutput(asub->valf, seg->rxNsec);

  // some metadata
  setOutput(asub->valg, seg->mode);
  setOutput(asub->valh, seg->count);
  setOutput(asub->vali, seg->leap);
  setOutput(asub->valj, seg->precision);
  setOutput(asub->valk, seg->nsamples);
  setOutput(asub->vall, seg->valid);

  // computed stuff
  double stampTime = combinedTime(seg->stampSec, seg->stampUsec, seg->stampNsec);
  double rxTime = combinedTime(seg->rxSec, seg->rxUsec, seg->rxNsec);
  double tDiffSeconds = seg->rxSec - seg->stampSec;
  double tDiffFraction = combinedTime(0, seg->rxUsec, seg->rxNsec) - combinedTime(0, seg->stampUsec, seg->stampNsec);
  double tDiff =  tDiffSeconds + tDiffFraction;
  setOutput(asub->valm, stampTime);
  setOutput(asub->valn, rxTime);
  setOutput(asub->valo, tDiff);
}
} // namespace ntpshm

int ntpshmRead(aSubRecord* asub) {
  try {
    ntpshm::setup();
    ntpshm::update(asub);
    return 0;
  } catch (const std::exception& e) {
    std::cerr << e.what() << "\n";
  } catch (...) {
    std::cerr << "unknown exception \n";
  }
  recGblSetSevr(asub, SOFT_ALARM, INVALID_ALARM);
  return 1;
}

epicsRegisterFunction(ntpshmRead);