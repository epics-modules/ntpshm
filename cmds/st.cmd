require ntpshm dev

epicsEnvSet("P", "Labs-TS:")
epicsEnvSet("R", "EVR-10:")

iocshLoad("$(ntpshm_DIR)/ntpshm.iocsh", "P=$(P), R=$(R)")
