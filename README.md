ntpshm
======
European Spallation Source ERIC Site-specific EPICS module : ntpshm

This module puts the information in a SHared Memory segment into PVs:
* Source time; at ESS from an MRF EVR
* Local system time
* Difference between the two

Additonal information:

* [Documentation](https://confluence.esss.lu.se/display/IS/Integration+by+ICS)
* [Release notes](RELEASE.md)
* [Requirements](https://gitlab.esss.lu.se/e3-recipes/ntpshm-recipe/-/blob/master/recipe/meta.yaml#L18)
* [Building and Testing](https://confluence.esss.lu.se/display/IS/1.+Development+locally+with+e3+and+Conda#id-1.Developmentlocallywithe3andConda-BuildanEPICSmoduleandtestitlocally)
